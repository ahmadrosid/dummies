<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', function () {

	dispatch(new \App\Jobs\LogSomething);

    return view('welcome');
});

Route::get('/profile', 'UserController@profile');
Route::post('/profile', 'UserController@update_avatar');

Route::get('/home', 'HomeController@index');

Route::get('/search/hotel', 'HotelController@index');
Route::post('/search/hotel', 'HotelController@index');

Route::get('/inspections', 'InspectionController@index');
Route::get('/inspections/new', 'InspectionController@create');
Route::get('/inspections/delete/{id}', 'InspectionController@deletes');
Route::post('/inspections/post', 'InspectionController@post');
