@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding:15px">
                    List Report
                    <a href="/inspections/new">
                        <button class="btn pull-right btn-primary btn-sm" >New Report</button>
                    </a>
                </div>

                @foreach($inspection as $data)
                <div class="panel-body"  style="background-color:#FFF">
                <div>
                    <img src="/uploads/inspections/{{$data->picture}}" style="width:250px; height:170px; float:left; margin-right:20px">
                    <div >
                        <h3 style="margin:0px">{{ $data->title }}</h3>
                        <h4 style="margin:5px 5px 5px 0px">{{ $data->company }}</h4>
                        <h5>{{ $data->description }}</h5>
                        <span>{{ $data->created_at }}</span>
                        <div style="margin-top:10px">
                            <a href="{{url('/inspections/delete') }}/{{$data->id}}" class="btn btn-danger btn-sm">Delete</a>
                        </div>
                    </div>
                </div>
                </div>
                    <div style="height:1px; margin-top:0px; background:#d4d4d4"></div>
                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection
